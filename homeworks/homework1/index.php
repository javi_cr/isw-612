<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="30"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Hola mundo de PHP</h1>
    <?php
        $fecha = new DateTime('NOW', new DateTimeZone('America/Costa_Rica'));
        echo "Hola esta es la hora:  ";
        echo $fecha->format('Y-m-d H:i:P') . "<br />";

        echo "Tu dirección IP es: {$_SERVER['REMOTE_ADDR']}";
    ?>

    <script language="javascript">
        setTimeout(function(){
        window.location.reload(1);
        }, 60000);
    </script>
</body>
</html>


