<?php

$rows    = filter_input(INPUT_GET, 'rows', FILTER_SANITIZE_STRING);
$columns = filter_input(INPUT_GET, 'columns', FILTER_SANITIZE_STRING);
$char    = filter_input(INPUT_GET, 'char', FILTER_SANITIZE_STRING);

function drawTable($rows, $columns, $char)
{
    $table = '<table border=1>';
    for ($i=0; $i < $rows; $i++) {
        $table .= drawRow($columns, $char);
    }
    $table .= '</table>';
    return $table;
}


function drawRow($columns, $char)
{
    $tr = '<tr>';
    for ($i=0; $i < $columns; $i++) {
        $tr .= "<td>$char</td>";
    }
    $tr .= '</tr>';
    return $tr;
}

?>

<form method="get" action="/dinamic.php">
    <label>Rows:</label>
    <input type="number" name="rows" required value="<?php echo $rows ?>">
    <br>
    <label>Columns:</label>
    <input type="number" name="columns" required value="<?php echo $columns ?>">
    <br>
    <label>Char:</label>
    <input type="text" name="char" required value="<?php echo $char ?>">
    <br>
    <input type="submit" name="">
</form>

<?php

echo drawTable($rows, $columns, $char);