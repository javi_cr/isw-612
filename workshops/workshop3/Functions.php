<?php

require_once 'DbConnection.php';

class Functions extends DbConnection
{
    private $connection;
    public function __construct($user, $password, $database, $port, $server)
    {
        parent::__construct($user, $password, $database, $port, $server);
    }

    public function connect()
    {
        $this->connection = new mysqli($this->server, $this->user, $this->password, $this->database);
    }
    public function disconnect()
    {
        $this->connection->close();
    }
    public function runQuery($sql, $params = [])
    {
        $stmt = $this->runStatement($sql, $params, false);
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    public function runStatement($sql, $params = [], $close_stmt = true)
    {
        $stmt = $this->connection->prepare($sql);
        if ($params) {
            $stmt->bind_param($this->dbTypes($params), ...$params);
        }
        $stmt->execute();
        if ($close_stmt) {
            $stmt->close();
        }
        return $stmt;
    }

    private function dbTypes($params)
    {
        $types = [
            'string' => 's',
            'integer' => 'i',
            'double' => 'd'
        ];
        $result = "";
        foreach ($params as $param) {
            $result .= $types[gettype($param)];
        }
        return $result;
    }
}